# CAN bus CTU FEE Projects

The list of CAN bus related projects at [Faculty of Electrical Engineering][ctu_fee] at [Czech Technical University in Prague][ctu_main] 

## CTU CAN FD IP Core

The VHDL open-source CAN FD core project.

Project pages: [https://gitlab.fel.cvut.cz/canbus/ctucanfd_ip_core](https://gitlab.fel.cvut.cz/canbus/ctucanfd_ip_core) 

Documentation and testing:

* [CTU CAN FD Core Datasheet Documentation](http://canbus.pages.fel.cvut.cz/ctucanfd_ip_core/doc/Datasheet.pdf)
* [CTU CAN FD Core System Architecture Documentation](http://canbus.pages.fel.cvut.cz/ctucanfd_ip_core/doc/System_Architecture.pdf)
* [CTU CAN FD Driver Documentation](http://canbus.pages.fel.cvut.cz/ctucanfd_ip_core/doc/linux_driver/build/ctucanfd-driver.html) ([Linux mainline](https://docs.kernel.org/networking/device_drivers/can/ctu/ctucanfd-driver.html))
* [The Core coverage by included test framework](http://canbus.pages.fel.cvut.cz/ctucanfd_ip_core/regression_results/coverage/)
* [Results of automatic test against test framework](https://gitlab.fel.cvut.cz/canbus/ctucanfd_ip_core/pipelines)
* [Results of automatic build by Xilinx Vivado and tests on Zynq-7000 MicroZed board]( https://gitlab.fel.cvut.cz/canbus/zynq/zynq-can-sja1000-top/pipelines)

Integration with FPGA SoCs and boards

* Integration with Zynq-7000 system - [https://gitlab.fel.cvut.cz/canbus/zynq/zynq-can-sja1000-top](https://gitlab.fel.cvut.cz/canbus/zynq/zynq-can-sja1000-top)

* Integration with Intel EP4CGX15 based DB4CGX15 PCIe board - [https://gitlab.fel.cvut.cz/canbus/pcie-ctu_can_fd](https://gitlab.fel.cvut.cz/canbus/pcie-ctu_can_fd)

* Integration with Cyclone V 5CSEMA4U23C6 based DE0-Nano-SoC Terasic board - [https://gitlab.fel.cvut.cz/canbus/intel-soc-ctucanfd](https://gitlab.fel.cvut.cz/canbus/intel-soc-ctucanfd)

Articles and Presentations

* Ille, O.; Novák, J.; Píša, P.; Vasilevski, M.: [CAN FD open-source IP core](https://can-newsletter.org/hardware/semiconductors/220819_can-fd-open-source-ip-core_czech-uni_cnlm), In:
[CAN Newsletter 3/2022](https://can-newsletter.org/uploads/media/raw/b5b652ff87e46689ffe4bddffcc352e2.pdf), [PDF](https://can-newsletter.org/uploads/media/raw/a9abe317ae034be55d99fee4410ad70e.pdf), CAN in Automation, 2022

* CTU CAN FD presented at [Workshop CAN in Satellites](https://old.can-cia.org/fileadmin/resources/documents/slides/2023_03_08_ws_can_in_satellites_minutes.pdf), CAN in Automation, 2023, slides 35 to 49

## OpenCores SJA-1000 FD Tol

[OpenCores](https://opencores.org/) [SJA-1000](https://opencores.org/projects/can) controller modified to ignore CAN FD frames which allows it to coexists and send frames on network with CAN FD traffic. The core is packed as a Xilinx Vivado component.

Project pages: [https://gitlab.fel.cvut.cz/canbus/zynq/sja1000-fdtol](https://gitlab.fel.cvut.cz/canbus/zynq/sja1000-fdtol)

##  CAN Bus Channels Mutual Latency Testing

Project done in cooperation with Voklswagen Research and [SocketCAN](https://github.com/linux-can) Author [Oliver Hartkopp](http://hartkopp.net/).

[Description of the project boards, hardware and VHDL desig](https://gitlab.fel.cvut.cz/canbus/zynq/zynq-can-sja1000-top/wikis/home).

[CAN/CAN FD Latency Tester utility](https://gitlab.fel.cvut.cz/canbus/can-benchmark/can-latester)

[CAN Latency Tester Automation](https://gitlab.fel.cvut.cz/canbus/can-benchmark/can-latester-automation)

[Daily Test Results](https://canbus.pages.fel.cvut.cz/can-latester/) with current [mainline](https://www.kernel.org/) Linux kernel and [linux-rt-devel](https://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-rt-devel.git/) (for-kbuild-bot/current-stable) RT varinat on [MZ_APO](https://cw.fel.cvut.cz/wiki/courses/b35apo/en/documentation/mz_apo/start) configured with [CTU CAN FD IP core](https://gitlab.fel.cvut.cz/canbus/ctucanfd_ip_core).

Articles and Presentations

* Píša, P.; Hronek, P.: [CAN Bus in Control, Automotive and Satellites](https://sched.co/1MYjG), [RedHat DevConf CZ 2023](https://www.devconf.info/), Brno, 2023, ([Video](https://youtu.be/RwmQYjfzQAg))

* Píša, P.; Hronek, P.; Vasilevski, M.; Novák, J.: [Continuous CAN Bus Subsystem Latency Evaluation and Stress Testing on GNU/Linux-Based Systems](files/ewc24-can-latester.pdf), In: [embedded world Conference 2024](https://events.weka-fachmedien.de/embedded-world-conference/). Haar: WEKA FACHMEDIEN GmbH, 2024. p. 77-82. ISBN 978-3-645-50199-6. ([Slides](files/ewc24-can-latester-slides.pdf))

## CAN/CAN FD Subsystem and Drivers for RTEMS
* [RTEMS CAN FD Project Sources](https://gitlab.fel.cvut.cz/otrees/rtems/rtems-canfd) ([in RTEMS mainline](https://gitlab.rtems.org/rtems/rtos/rtems/-/tree/main/cpukit/dev/can))
* [RTEMS CAN FD Documentation - Manual](https://otrees.pages.fel.cvut.cz/rtems/rtems-canfd/doc/can/can-html/can.html) ([in RTEMS mainline](https://docs.rtems.org/docs/main/bsp-howto/can.html))
* [RTEMS CAN FD Source Documentation](https://otrees.pages.fel.cvut.cz/rtems/rtems-canfd/doc/doxygen/html/index.html)
* [RTEMS Real Time Operating System](https://www.rtems.org/)

Articles and Presentations

* Lenc, M.; Píša, P.: [Scheduling of CAN frame transmission when multiple FIFOs with assigned priorities are used in RTOS drivers](https://www.can-cia.org/fileadmin/cia/documents/proceedings/2024_lenc_pisa.pdf), [international CAN Conference](https://www.can-cia.org/icc/), [CAN in Automation](https://can-cia.org/), 2024. ([Slides](files/scheduling_of_can_message_transmission_michal_lenc.pdf))

* Lenc, M.: [CAN FD Support for Space Grade Real-Time RTEMS Executive](https://wiki.control.fel.cvut.cz/mediawiki/images/c/cc/Dp_2024_lenc_michal.pdf), Master's Thesis, CTU, 2024

## List of more CTU FEE CAN projects

* [LinCAN driver][1] ([PDF document][2])
* [OCERA CANOpen framework][ortcan_canopen] - [OCERA CANopen User Guide][ocera-canopen-ug]
* [OrtCAN - Open/OCERA real-time CAN components][ortcan] - offshoot of original OCERA project.
  Project related notices by [PavelPisa][4]
* [Benchmarks of LinCAN and Socketcan drivers][5]
* [Benchmarks of SocketCAN-based gateway][cangw]
* [SocketCAN and queueing disciplnes (technical report, PDF)](https://rtime.felk.cvut.cz/can/socketcan-qdisc-final.pdf)
* [LIN-bus][lin_bus_wiki] support and tools for Linux and SocketCAN (sllin, etc.) [GitHub repository][linux_lin_git]
* [CAN hardware emulation in QEMU][can-qemu], [CAN for QEMU presentation][linuxdays_can4qemu] and related [GitLab repository][can_qemu_git] integrated into mainline [DOC][can_qemu_doc]
* [Simulink Blocks for CAN bus Support under Linux][simulink_socketcan]
* [Performance evaluation of Linux CAN-related system calls](https://rtime.felk.cvut.cz/can/can-syscalls.pdf) ([example code](https://rtime.felk.cvut.cz/gitweb/sojka/can-syscalls-examples.git/tree/HEAD))
* [Linux-Based CAN-Ethernet Gateway](https://rtime.felk.cvut.cz/can/can-eth-gw.pdf)
* Diploma and other theses related to CAN bus at [CTU Faculty of Electrical Engineering][ctu_fee]
    * [Pavel Pičman: Mobile CAN bus analyzer, 2005][picman_dp] ([PDF CZ][picman_pdf])
    * [Miroslav Černý: Communication CAN in vehicle, 2005][cerny_dp] ([PDF CZ][cerny_pdf])
    * [Stanislav Marek: Embedded Device with CANopen Communication Protocol, 2006][marek_dp] ([PDF CZ][marek_pdf])
    * [Jan Benda: CANOpen Communication for a Mobile Robot, 2008][benda_dp] ([PDF CZ][benda_pdf])
    * [Martin Petera: LinCAN driver for Renesas HCAN2, 2007][petera_bp] ([PDF CZ][petera_bp_pdf])
    * [Martin Petera: Implementation of the LinCAN Driver for the PowerPC Platform and LinCAN-SocketCAN Evaluation, 2010][petera_dp] ([PDF CZ][petera_dp_pdf])
    * [Miloš Gajdoš: CAN bus communication protocol support and monitoring, 2008][gajdos_dp] ([PDF EN][gajdos_pdf])
    * [Marek Peca: Two-legged robot, 2008][peca_dp] ([PDF CZ][peca_pdf])
    * [Jan Kříž: Implementation of USB-CAN converter support for LinCAN driver, 2008][kriz_bp] ([PDF CZ][kriz_pdf]) ([Git][14])
    * [Jiří Zemánek: CAN board with Renesas H8S/2638 microcontroller, 2006][zemanek_dp] ([PDF CZ][zemanek_pdf]) [HW Zoo][15] [Board documentation][16]
    * [Lukáš Hamáček: RTW target for Linux with CANOpen support, 2009][hamacek_dp] ([PDF EN][hamacek_pdf]), part of Libor Waszniowski lead projects
    * [Tomáš Barák: Remote access to CAN bus over Ethernet, 2010][barak_dp] ([PDF][barak_pdf]) ([Git][barak_git])
    * [Košarko Martin: CAN BUS analyzer with Ethernet interface, 2012][kosarko_dp] ([PDF CZ][kosarko_pdf])
    * [Semmler Ondřej: Firmware of USB/CAN Gateway, 2012][semler_dp] ([PDF CZ][semler_pdf])
    * [Jiří Vaněk: CAN Bus to USB Converter for GNU/Linux, 2012][vanek_dp] ([PDF][vanek_pdf]) ([LinCAN Devel Git, branch lpc17xx][lincan_dev_git]) ([SocketCAN converter support, branch vanekjir][socketcan_dev_git])
    * [Fridrich Ivan: Tools for CANopen dictionaries and their integration into embedded systems, 2012][fridrich_dp] ([PDF][fridrich_pdf]) ([qCANalyzer][qcanalyzer_www]) ([qCANalyzer Git][qcanalyzer_git])
    * [Ondřej Kulatý: Message authentication for CAN bus and AUTOSAR software architecture, 2015][kulaty_dp] ([PDF EN][kulaty_dp_pdf])
    * [Martin Jeřábek: FPGA Based CAN Bus Channels Mutual Latency Tester and Evaluation, 2016][jerabek_bp] ([PDF EN][jerabek_bp_pdf]) ([CAN-BENCH_Schematics][jerabek_bp_sch])
    * [Martin Prudek: Enhancing Raspberry Pi Target for Simulink to Meet Real-Time Latencies, 2017][prudek_dp] ([PDF EN][prudek_dp_pdf])
    * [Fitz Karel: Extension of the Existing CAN Controller for FD Standard, 2017][fitz_dp] ([PDF CZ][fitz_pdf])
    * [Martin Hofman: Time-Triggered Scheduler Implementation in Distributed Safety-Critical Control System, 2017][hofman_dp] ([PDF CZ][hofman_dp_pdf])
    * [Martin Jeřábek: Open-source and Open-hardware CAN FD Protocol Support, 2019][jerabek_dp] ([PDF EN][jerabek_dp_pdf])
    * [Jan Charvát: Model of CAN FD Communication Controller for QEMU Emulator, 2020][charvat_bp] ([PDF EN][charvat_bp_pdf])
    * [Jaroslav Beran: Firmware for Control Module of an Intelligent Vehicle, 2020][beran_dp] ([PDF EN][beran_dp_pdf])
    * [Jan Charvát: NuttX RTOS CAN Bus Driver for Espressif ESP32C3, 2022][charvat_dp] ([PDF EN][charvat_dp_pdf])
    * [Matěj Vasilevski: CAN Bus Latency Test Automation for Continuous Testing and Evaluation, 2022][vasilmat_dp] ([PDF EN][vasilmat_dp_pdf])
    * [Pavel Hronek: CAN Bus Latency Test Automation for Continuous Testing and Evaluation, 2023][hronek_bp] ([PDF EN][hronek_bp_pdf])
* Other related project realized at [Department of Control Engineering][dce_fel]
    * [Libor Waszniowski: Hardware in the Loop Simulation of the Yaw Dumper][17]
    * [Libor Waszniowski: Linux Target for Code Generation by Real Time Workshop][18] ([Source Forge project][19]),

  [ctu_main]: https://www.cvut.cz/en/
  [ctu_fee]: https://fel.cvut.cz/en/
  [1]: https://ortcan.sourceforge.net/lincan/
  [2]: http://cmp.felk.cvut.cz/~pisa/can/doc/lincandoc-0.3.pdf
  [ortcan]: http://ortcan.sourceforge.net/
  [ortcan_canopen]: http://ortcan.sourceforge.net/vca/
  [4]: http://cmp.felk.cvut.cz/~pisa/#can
  [5]: http://rtime.felk.cvut.cz/can/benchmark/1/
  [13]: http://dce.felk.cvut.cz/spejbl/
  [14]: https://sourceforge.net/p/ortcan/lincan/ci/can-usb1/tree/
  [15]: http://dce.felk.cvut.cz/nms/?p=hardware#h8canusb
  [16]: http://rtime.felk.cvut.cz/hw/index.php/H8canusb
  [17]: http://support.dce.felk.cvut.cz/pub/xwasznio/HIL_YDC/HIL_YDC.htm
  [18]: http://lintarget.sourceforge.net/
  [19]: http://sourceforge.net/projects/lintarget/
  [dce_fel]: https://control.fel.cvut.cz/en
  [marek_dp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Dp_218_en
  [marek_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/3/37/Dp_2006_marek_stanislav.pdf
  [benda_dp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Dp_258_en
  [benda_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/e/e4/Dp_2008_benda_jan.pdf
  [petera_bp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Bp_130_en
  [petera_bp_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/0/07/Bp_2007_petera_martin.pdf
  [petera_dp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Dp_382_en
  [petera_dp_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/f/f8/Dp_2010_petera_martin.pdf
  [gajdos_dp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Dp_264_en
  [gajdos_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/d/d2/Dp_2008_gajdos_milos.pdf
  [peca_dp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Dp_297_en
  [peca_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/d/d7/Dp_2008_peca_marek.pdf
  [kriz_bp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Bp_205_en
  [kriz_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/1/13/Bp_2008_kriz_jan.pdf
  [zemanek_dp]: https://dip.felk.cvut.cz/browse/details.php?f=F3&d=K13136&y=2006&a=zemanej&t=dipl
  [zemanek_pdf]: https://dip.felk.cvut.cz/browse/pdfcache/zemanej_2006dipl.pdf
  [hamacek_dp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Dp_340_en
  [hamacek_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/5/5b/Dp_2009_hamacek_lukas.pdf
  [barak_dp]: http://dip.felk.cvut.cz/browse/details.php?f=F3&d=K13136&y=2010&a=baraktom&t=bach
  [barak_pdf]: http://dip.felk.cvut.cz/browse/pdfcache/baraktom_2010bach.pdf
  [barak_git]: http://cgit.spermik.info/cgit/arm-lpc/
  [picman_dp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Dp_202_en
  [picman_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/c/c9/Dp_2005_picman_pavel.pdf
  [cerny_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/4/41/Dp_2005_cerny_miroslav.pdf
  [cerny_dp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Dp_181_en
  [vanek_dp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Dp_490_en
  [vanek_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/5/56/Dp_2012_vanek_jiri.pdf
  [fridrich_dp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Dp_477_en
  [fridrich_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/3/3f/Dp_2012_fridrich_ivan.pdf
  [kosarko_dp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Dp_466_en
  [kosarko_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/d/d2/Dp_2012_kosarko_martin.pdf
  [semler_dp]: http://support.dce.felk.cvut.cz/mediawiki/index.php/Dp_486_en 
  [semler_pdf]: http://support.dce.felk.cvut.cz/mediawiki/images/0/0b/Dp_2012_semmler_ondrej.pdf
  [cangw]: http://rtime.felk.cvut.cz/can/benchmark/3.0/
  [cangw-old]: http://rtime.felk.cvut.cz/can/benchmark/2/
  [lincan_dev_git]: https://sourceforge.net/p/ortcan/lincan/
  [socketcan_dev_git]: http://rtime.felk.cvut.cz/gitweb/socketcan-devel.git
  [qcanalyzer_www]: http://ortcan.sourceforge.net/qcanalyzer/
  [qcanalyzer_git]: https://sourceforge.net/p/ortcan/qcanalyzer/
  [can_qemu_git]: https://gitlab.fel.cvut.cz/canbus/qemu-canbus
  [simulink_socketcan]: http://lintarget.sourceforge.net/can_bus/index.html
  [fitz_dp]: https://dspace.cvut.cz/handle/10467/68543
  [fitz_pdf]: https://dspace.cvut.cz/bitstream/handle/10467/68543/F3-DP-2017-Fitz-Karel-Navrh%20rozsireni%20existujiciho%20radice%20CAN%20o%20standard%20FD.pdf
  [jerabek_dp]: https://dspace.cvut.cz/handle/10467/80366
  [jerabek_dp_pdf]: https://dspace.cvut.cz/bitstream/handle/10467/80366/F3-DP-2019-Jerabek-Martin-Jerabek-thesis-2019-canfd.pdf
  [linux_lin_git]: https://github.com/lin-bus/linux-lin
  [lin_bus_wiki]: https://github.com/lin-bus/linux-lin/wiki
  [can_qemu_doc]: https://www.qemu.org/docs/master/system/devices/can.html
  [linuxdays_can4qemu]:https://www.linuxdays.cz/2017/video/Pavel_Pisa-CAN_canopen.pdf
  [can-qemu]:http://cmp.felk.cvut.cz/~pisa/can/doc/rtlws-17-pisa-qemu-can.pdf
  [ocera-canopen-ug]:http://cmp.felk.cvut.cz/~pisa/can/doc/ocera-canopen-ug.pdf
  [kulaty_dp]:https://dspace.cvut.cz/handle/10467/61250
  [kulaty_dp_pdf]:https://dspace.cvut.cz/bitstream/handle/10467/61250/F3-DP-2015-Kulaty-Ondrej-kulatond-thesis-2014.pdf
  [jerabek_bp]:https://gitlab.fel.cvut.cz/canbus/zynq/zynq-can-sja1000-top/wikis/home#can-bench-board-and-design
  [jerabek_bp_pdf]:https://gitlab.fel.cvut.cz/canbus/zynq/zynq-can-sja1000-top/wikis/uploads/56b4d27d8f81ae390fc98bdce803398f/F3-BP-2016-Jerabek-Martin-Jerabek-thesis-2016.pdf
  [jerabek_bp_sch]:https://gitlab.fel.cvut.cz/canbus/zynq/zynq-can-sja1000-top/wikis/uploads/93b79611ed12cfb806fcefc6f86973fc/F3-BP-2016-Jerabek-Martin-priloha-CAN-BENCH_Schematics_RevA.pdf
  [hofman_dp]: https://dspace.cvut.cz/handle/10467/70108
  [hofman_dp_pdf]: https://dspace.cvut.cz/bitstream/handle/10467/70108/F3-DP-2017-Hofman-Martin-Implementace_casem_rizeneho_planovace_v_distribuovanem_bezpecnostne_kritickem_ridicim_systemu.pdf
  [prudek_dp]:https://dspace.cvut.cz/handle/10467/68605
  [prudek_dp_pdf]:https://dspace.cvut.cz/bitstream/handle/10467/68605/F3-DP-2017-Prudek-Martin-Dp_2017_prudek_martin.pdf?sequence=1&isAllowed=y
  [charvat_bp]:https://dspace.cvut.cz/handle/10467/87714
  [charvat_bp_pdf]:https://dspace.cvut.cz/bitstream/handle/10467/87714/F3-BP-2020-Charvat-Jan-Model_of_CAN_FD_Communication_Controller_for_QEMU_Emulator.pdf
  [beran_dp]:https://dspace.cvut.cz/handle/10467/89957
  [beran_dp_pdf]:https://dspace.cvut.cz/bitstream/handle/10467/89957/F3-DP-2020-Beran-Jaroslav-beranj25_thesis.pdf
  [charvat_dp]:https://dspace.cvut.cz/handle/10467/101692
  [charvat_dp_pdf]:https://dspace.cvut.cz/bitstream/handle/10467/101692/F3-DP-2022-Charvat-Jan-NuttX-RTOS-CAN-Bus-Driver-for-Espressif-ESP32C3.pdf
  [vasilmat_dp]:https://dspace.cvut.cz/handle/10467/101450
  [vasilmat_dp_pdf]:https://dspace.cvut.cz/bitstream/handle/10467/101450/F3-DP-2022-Vasilevski-Matej-vasilmat.pdf
  [hronek_bp]: https://dspace.cvut.cz/handle/10467/109308
  [hronek_bp_pdf]: https://dspace.cvut.cz/bitstream/handle/10467/109308/F3-BP-2023-Hronek-Pavel-CAN-Latester-Automation.pdf
